let buttons = document.querySelectorAll('#buttons');                     //Intercetto i 9 bottoni creando una nodelist.
const playerVScpu = document.querySelector('.PvC');                      //Intercetto il bottone per giocare contro il computer.
const twoPlayers = document.querySelector('.TP');                        //Intercetto il bottone per giocare con due giocatori.
const hard = document.querySelector('.HrD');                             //Intercetto il bottone per giocare contro il computer in modalità "hard".
const border = document.querySelector('.customContainer');               //Intercetto la classe inerente al bordo per poterlo rendere personalizzabile nella modalità "impossible".
const log = document.querySelector('#log');                              //Intercetto la finestra di dialogo.
const clearText = document.querySelector('.clearText');                  //Intercetto l'icona del cestino.
const readInfo = document.querySelector('.readInfo');                    //Intercetto l'icona della "i".
let userMove = true;                                                     //Variabile booleana che permette di gestire i turni tra utente e computer o tra i 2 giocatori.
let firstCPUmove = true;                                                 //Variabile booleana che gestisce la prima mossa del computer.
let secondCPUmove = false;                                               //Variabile booleana che gestisce la seconda mossa del computer.
let hardMode = false;                                                    //Variabile booleana di supporto per la modalità "hard".

const makeAmove = e => {                                                 //Salvo in una costante la funzione che permette di riempire le caselle all'utente: necessario per poter rendere l'eventListener cancellabile quando si esce dalla modalità di gioco selezionata.
    if (e.target.textContent != ""){                                     //Se la casella selezionata è già segnata da un simbolo
        let div = document.createElement('div');                         
        log.append(div);
        div.innerHTML = "You cannot tick a box already marked.";         //scrivi questo messaggio.
    } else if (e.target.textContent == ""){                              ////Altrimenti, se la casella selezionata è vuota,
        e.target.setAttribute('style' , 'color: greenyellow;');          //metti colore e simbolo,
        e.target.innerHTML = "X";
        userMove = false;                                                //abilita il computer a fare la sua mossa e
        playerVScomputer();                                              //ritorna nella funzione playerVScomputer.
    }
}

const player1moves = e => {                                              //Salvo in una costante la funzione che permette di riempire le caselle al giocatore1:
    if (e.target.textContent != ""){                                     //necessario per poter rendere l'eventListener cancellabile quando si esce dalla modalità
        let div = document.createElement('div');                         //di gioco selezionata.
        log.append(div);
        div.innerHTML = "You cannot tick a box already marked.";         //Scrivi questo messaggio se la casella è già occupata,
    } else {
        e.target.setAttribute('style' , 'color: greenyellow;');          //altrimenti metti colore e simbolo.      
        e.target.innerHTML = "X";
        userMove = false;                                                    //Inverto la variabile per far giocare il giocatore2.
        playerVSplayer();                                                //Ritorno nella funzione playerVSplayer.
    }
}

const player2moves = e => {                                              //Salvo in una costante la funzione che permette di riempire le caselle al giocatore2:
    if (e.target.textContent != ""){                                     //necessario per poter rendere l'eventListener cancellabile quando si esce dalla modalità
        let div = document.createElement('div');                         //di gioco selezionata.
        log.append(div);
        div.innerHTML = "You cannot tick a box already marked.";         //Scrivi questo messaggio se la casella è già occupata,
    } else {
        e.target.setAttribute('style' , 'color: red;');                  //altrimenti metti colore e simbolo. 
        e.target.innerHTML = "O";
        userMove = true;                                                     //Inverto la variabile per far giocare il giocatore1.   
        playerVSplayer();                                                //Ritorno nella funzione playerVSplayer.
    }
}

function resetButtons() {                                                //Creo una specie di funzione di
    buttons.forEach(button => {                                          //reset per cancellare tutti i bottoni.
        button.removeAttribute('style');
        button.innerHTML = "";
    });
}

function selectGame() {                                                  //Creo la funzione che permette di selezionare il tipo di gioco.
    resetButtons();                                                      //Resetto i bottoni.
    playerVScpu.removeAttribute('style');
    twoPlayers.removeAttribute('style');
    hard.removeAttribute('style');
    border.removeAttribute('style');                                     //Rimuovo il bordo colorato dalla schermata,
    border.setAttribute('style' , 'border: 5px solid yellow;');          //per poterlo riprisitinare in colore giallo.

    playerVScpu.removeEventListener('click', selectGame);                //Rimuovo l'eventListener su funzione selectGame
    twoPlayers.removeEventListener('click', selectGame);                 //per i 3 bottoni modalità gioco.
    hard.removeEventListener('click', selectGame);

    buttons.forEach(button => {                                          //Non sapendo da quale funzione sto uscendo, per ogni singolo bottone,
        button.removeEventListener('click', makeAmove);                  //rimuovo tutti gli eventListener possibili.
        button.removeEventListener('click', player1moves);
        button.removeEventListener('click', player2moves); 
    })
    firstCPUmove = true;                                                 //Rimetto a true la variabile booleana per far fare al computer la prima mossa a random.
    secondCPUmove = false;                                               //Rimetto a false la variabile booleana per non far fare al computer la seconda mossa.
    hardMode = false;                                                    //Rimetto a false la variabile booleana per non far fare al computer la prima mossa.
    userMove = true;                                                     //Rimetto a true la variabile booleana per far sempre giocare per primo l'utente.
    playerVScpu.addEventListener('click', playerVScomputer);             //Su questo bottone metto l'eventListener per avviare la partita contro il computer.
    twoPlayers.addEventListener('click', playerVSplayer);                //Su questo bottone metto l'eventListener per avviare la partita contro un altro giocatore.
    hard.addEventListener('click', engageHardMode);                      //Su questo bottone metto l'eventListener per avviare la partita contro il computer in modalità "hard".   

    let div = document.createElement('div');          
    log.append(div);
    div.innerHTML = "Select mode ↑↑";
}
////////////////////////////////////PLAYER VS CPU////////////////////////////////////////////////
function engageHardMode(){                      //Questa funzione impedisce al giocatore di fare la prima mossa
    hardMode = true;                            //qualora fosse selezionata la modalità "hard".
    userMove = false;
    playerVScomputer();
}

function playerVScomputer(){                                             //Funzione principale della modalità PLAYER VS CPU.
    if (hardMode == false){                                              //Se è selezionata la modalità di gioco "giocatore VS computer":
        hard.removeAttribute('style');                             //rimuovo il bordo dal bottone della modalità "hard" e
        playerVScpu.setAttribute('style' , 'border: 3px solid yellow;'); //aggiungo il bordo per indicare all'utente la modalità selezionata.     
    } else {                                                             //Altrimenti, se è selezionata la modalità "hard":
        playerVScpu.removeAttribute('style');                            //rimuovo il bordo dal bottone della modalità "giocatore vs computer",
        hard.setAttribute('style' , 'border: 3px solid red;');     //aggiungo il bordo per indicare all'utente la modalità selezionata.
        border.removeAttribute('style');                                 //Rimuovo il bordo colorato dalla schermata,
        border.setAttribute('style' , 'border: 5px solid red;');         //per poi renderlo rosso nella modalità "hard".
    }
    twoPlayers.removeAttribute('style');                                 //Rimuovo il bordo dal bottone modalità a 2 giocatori.
                                                                                      
    playerVScpu.removeEventListener('click', playerVScomputer);          //Su questo bottone rimuovo l'eventListener per avviare la partita contro il computer.
    twoPlayers.removeEventListener('click', playerVSplayer);             //Su questo bottone rimuovo l'eventListener per avviare la partita contro un altro giocatore.
    hard.removeEventListener('click', engageHardMode);

    playerVScpu.addEventListener('click', selectGame);                   //Su entrambi i bottoni metto l'eventListener per tornare alla selezione della partita.
    twoPlayers.addEventListener('click', selectGame);
    hard.addEventListener('click', selectGame);

    buttons.forEach(button => {                                          //Per ogni bottone,                 
        button.addEventListener('click', makeAmove);                     //attacco l'eventListener che chiama la fuznione "makeAmove".
    })
    if (GameCheck(buttons) == "unknown"){                                //Effettuo un controllo per vedere se la partita è ancora in corso.                         
        if (firstCPUmove == true && secondCPUmove == false && userMove == false){//Il computer deve eseguire la seconda mossa completamente casuale:
            let i = getRandomIntInclusive(0, 8);                         //estraggo un numero casuale dal range 0-8,
            while (buttons[i].textContent != ""){                        //controllo se quella casella è già occupata,
                i = getRandomIntInclusive(0, 8);                         //se è già occupata rieseguo l'estrazione del numero casuale.
            }
            buttons[i].setAttribute('style' , 'color: red;');            //Aggiungo colore e simbolo nella casella scelta.
            buttons[i].innerHTML = "O";
            firstCPUmove = false;                                        //Disabilito la prima mossa del computer e 
            secondCPUmove = true;                                        //abilito la seconda mossa del computer.
            userMove = true;                                             //Rimetto a true per far giocare l'utente.               
        }
        if (firstCPUmove == false && secondCPUmove == true && userMove == false){//La seconda mossa del computer controlla se l'utente sta usando la strategia
            let i = checkSecondUserMove(buttons);                        //a triangolo, se risulta vero allora interviene, altrimenti 
            buttons[i].setAttribute('style' , 'color: red;');            //segue la logica di controllo ordinaria (controlLogic).
            buttons[i].innerHTML = "O";
            secondCPUmove = false;                                       //Disabilito la seconda mossa del computer.
            userMove = true;                                             //Rimetto a true per far giocare l'utente. 
        }  
        if (firstCPUmove == false && secondCPUmove == false && userMove == false){    //Le altre mosse del computer seguono la logica delle due funzioni "controlLogic" e "progressLogic":
            let i = controlLogic(buttons);                               //estraggo l'indice dalla funzione controlLogic che manda come parametro tutta la nodeList.
            buttons[i].setAttribute('style' , 'color: red;');            //aggiungo colore e simbolo nella casella scelta.
            buttons[i].innerHTML = "O";
            userMove = true;           
            if ((GameCheck(buttons) == "computer") || (hardMode == true)){ //Effettuo un controllo per vedere se il computer ha vinto oppure c'è un pareggio alla fine.
                playerVScomputer();
            }            
        }
    } else {
        buttons.forEach(button => {                                     //In caso di vincita rimuovo gli eventListener dai bottoni.
            button.removeEventListener('click', makeAmove);
        })
        let div = document.createElement('div');
        switch (GameCheck(buttons)){                                    //In base al tipo di risultato ottenuto, stampo i vari messaggi.
            case "player":                                              //Le stesse condizioni sono presenti nella funzione playerVSplayer.
                log.append(div);
                div.innerHTML = "You won! :)";
                break;
            case "computer":
                log.append(div);
                div.innerHTML = "Computer won. U.U";
                break;
            case "noone":
                log.append(div);
                div.innerHTML = "Uhm, it's a draw. :/";
                break;
        }
    }
}    

function checkSecondUserMove(a){        //Questa funzione serve per permettere al computer di controllare qualora l'utente stia seguendo                esempio di strategia a "triangolo": | |X| |          | | | |
    let e;                              //la strategia a "triangolo", se il riscontro è positivo allora il computer interviene,                                                             | | | |  oppure  | | | |
    switch (true){                      //                                                                                                                                                  |X| | |          |X|O|X|
        case (a[6].textContent == "X" && a[1].textContent == "X" && a[2].textContent == "" && a[4].textContent == "" && a[7].textContent == ""):        //| |X|?|
            e = getRandomIntInclusive(2, 7);                                                                                                            //| |?| |                                     
            while ((e == 3) || (e == 5) || (e == 6)){                                                                                                   //|X|?| |
                e = getRandomIntInclusive(2, 7);
            }                                                                                                                       
            break;
        case (a[8].textContent == "X" && a[1].textContent == "X" && a[0].textContent == "" && a[4].textContent == "" && a[7].textContent == ""):        //|?|X| |
            e = getRandomIntInclusive(0, 7);                                                                                                            //| |?| |                                     
            while ((e == 1) || (e == 2) || (e == 3) || (e == 5) || (e == 6)){                                                                           //| |?|X|
                e = getRandomIntInclusive(0, 7);
            }                                                                                                                       
            break;
        case (a[0].textContent == "X" && a[5].textContent == "X" && a[3].textContent == "" && a[4].textContent == "" && a[8].textContent == ""):        //|X| | |
            e = getRandomIntInclusive(3, 8);                                                                                                            //|?|?|X|
            while ((e == 5) || (e == 6) || (e == 7)){                                                                                                   //| | |?|
                e = getRandomIntInclusive(3, 8);                              
            }                                                                                                         
            break;
        case (a[6].textContent == "X" && a[5].textContent == "X" && a[2].textContent == "" && a[3].textContent == "" && a[4].textContent == ""):        //| | |?|
            e = getRandomIntInclusive(2, 4);                                                                                                            //|?|?|X|                                                                                                    
            break;                                                                                                                                      //|X| | |

        case (a[2].textContent == "X" && a[7].textContent == "X" && a[1].textContent == "" && a[4].textContent == "" && a[6].textContent == ""):        //| |?|X|
            e = getRandomIntInclusive(1, 6);                                                                                                            //| |?| |
            while ((e == 2) || (e == 3) || (e == 5)){                                                                                                   //|?|X| |
                e = getRandomIntInclusive(1, 6);  
            }                                                                               
            break;
        case (a[0].textContent == "X" && a[7].textContent == "X" && a[1].textContent == "" && a[4].textContent == "" && a[8].textContent == ""):        //|X|?| |
            e = getRandomIntInclusive(1, 8);                                                                                                            //| |?| |
            while ((e == 2) || (e == 3) || (e == 5) || (e == 6) || (e == 7)){                                                                           //| |X|?|
                e = getRandomIntInclusive(1, 8);  
            }                                                                               
            break;
        case (a[2].textContent == "X" && a[3].textContent == "X" && a[4].textContent == "" && a[5].textContent == "" && a[6].textContent == ""):        //| | |X|
            e = getRandomIntInclusive(4, 6);                                                                                                            //|X|?|?|                                                                                                    
            break;                                                                                                                                      //|?| | |

        case (a[8].textContent == "X" && a[3].textContent == "X" && a[0].textContent == "" && a[4].textContent == "" && a[5].textContent == ""):        //|?| | |
            e = getRandomIntInclusive(0, 5);                                                                                                            //|X|?|?|
            while ((e == 1) || (e == 2) || (e == 3)){                                                                                                   //| | |X|
                e = getRandomIntInclusive(0, 5); 
            }                                                                                                      
            break;
        case ((a[0].textContent == "X" && a[3].textContent == "O" && a[6].textContent == "X" && a[4].textContent == "") ||      //|X| | |     |X|O|X|     | | |X|     | | | |
             (a[0].textContent == "X" && a[1].textContent == "O" && a[2].textContent == "X" && a[4].textContent == "") ||       //|O|?| |     | |?| |     | |?|O|     | |?| |
             (a[2].textContent == "X" && a[5].textContent == "O" && a[8].textContent == "X" && a[4].textContent == "") ||       //|X| | |     | | | |     | | |X|     |X|O|X|
             (a[6].textContent == "X" && a[7].textContent == "O" && a[8].textContent == "X" && a[4].textContent == "")):
            e = 4;
            break;
        case ((a[6].textContent == "X" && a[4].textContent == "O" && a[2].textContent == "X") ||                                //| |?|X|     |X|?| |   
             (a[0].textContent == "X" && a[4].textContent == "O" && a[8].textContent == "X")):                                  //|?|O|?|     |?|O|?|
            e = getRandomIntInclusive(1, 7);                                                                                    //|X|?| |     | |?|X|                                              
            while ((e == 2) || (e == 4) || (e == 6)){                                                                                                   
                e = getRandomIntInclusive(1, 7); 
            } 
            break;
        default:
            e = controlLogic(a);        //altrimenti passa direttamente alla logica di controllo standard.
            break;
    }
    return e;
}

function checkMovesMade(a, b){       //Questa funzione serve per verificare le mosse fatte dal computer o dal giocatore: se trova una casella vuota contigua a due caselle segnate con lo stesso simbolo
    let e;                           //del parametro passato alla funzione (X oppure O), allora fa uscire l'indice della casella vuota che servirà per la funzione controlLogic.
    switch (true){                                                                                 //horizontal   vertical   diagonal   diagonal(reverse)                                                                                                                              
        case ((a[1].textContent == b && a[2].textContent == b) ||                                  //| |b|b|      | |?|?|    | |?|?| 
            (a[3].textContent == b && a[6].textContent == b) ||                                    //|?|?|?|      |b|?|?|    |?|b|?|
            (a[4].textContent == b && a[8].textContent == b)) && (a[0].textContent == ""): e=0;    //|?|?|?|      |b|?|?|    |?|?|b|
            break;
        case ((a[0].textContent == b && a[2].textContent == b) ||                                  //|b| |b|      |?| |?|
            (a[4].textContent == b && a[7].textContent == b)) && (a[1].textContent == ""): e=1;    //|?|?|?|      |?|b|?|
            break;                                                                                 //|?|?|?|      |?|b|?|

        case ((a[0].textContent == b && a[1].textContent == b) ||                                  //|b|b| |      |?|?| |    |?|?| |
            (a[5].textContent == b && a[8].textContent == b) ||                                    //|?|?|?|      |?|?|b|    |?|b|?|
            (a[4].textContent == b && a[6].textContent == b)) && (a[2].textContent == ""): e=2;    //|?|?|?|      |?|?|b|    |b|?|?|
            break;
        case ((a[0].textContent == b && a[6].textContent == b) ||                                  //|?|?|?|      |b|?|?|
            (a[4].textContent == b && a[5].textContent == b)) && (a[3].textContent == ""): e=3;    //| |b|b|      | |?|?|
            break;                                                                                 //|?|?|?|      |b|?|?|
        case ((a[3].textContent == b && a[5].textContent == b) ||
            (a[1].textContent == b && a[7].textContent == b) ||                                    //|?|?|?|      |?|b|?|    |b|?|?|    |?|?|b|
            (a[0].textContent == b && a[8].textContent == b) ||                                    //|b| |b|      |?| |?|    |?| |?|    |?| |?|
            (a[2].textContent == b && a[6].textContent == b)) && (a[4].textContent == ""): e=4;    //|?|?|?|      |?|b|?|    |?|?|b|    |b|?|?|
            break;
        case ((a[2].textContent == b && a[8].textContent == b) ||                                  //|?|?|?|      |?|?|b|
            (a[3].textContent == b && a[4].textContent == b)) && (a[5].textContent == ""): e=5;    //|b|b| |      |?|?| |
            break;                                                                                 //|?|?|?|      |?|?|b|

        case ((a[0].textContent == b && a[3].textContent == b) ||                                  //|?|?|?|      |b|?|?|    |?|?|b|
            (a[7].textContent == b && a[8].textContent == b) ||                                    //|?|?|?|      |b|?|?|    |?|b|?|
            (a[4].textContent == b && a[2].textContent == b)) && (a[6].textContent == ""): e=6;    //| |b|b|      | |?|?|    | |?|?|
            break;
        case ((a[6].textContent == b && a[8].textContent == b) ||                                  //|?|?|?|      |?|b|?|
            (a[4].textContent == b && a[1].textContent == b)) && (a[7].textContent == ""): e=7;    //|?|?|?|      |?|b|?|
            break;                                                                                 //|b| |b|      |?| |?|

        case ((a[6].textContent == b && a[7].textContent == b) ||                                  //|?|?|?|      |?|?|b|    |b|?|?|
            (a[2].textContent == b && a[5].textContent == b) ||                                    //|?|?|?|      |?|?|b|    |?|b|?|
            (a[0].textContent == b && a[4].textContent == b)) && (a[8].textContent == ""): e=8;    //|b|b| |      |?|?| |    |?|?| |
            break;
        default:
            e = null;    //Se non c'è nessuna casella vuota adiacente a due caselle col simbolo "X" oppure "O",
            break;       //la funzione restituisce null.
    }
    return e;
}

function controlLogic(a){            //Questa funzione controlla i valori in uscita dalla funzione "checkMovesMade".
    let e;
    const O = "O";
    const X = "X";
    const cpu = checkMovesMade(a, O);
    const player = checkMovesMade(a, X);
    switch (true){
        case ((cpu == null) && (player != null)):   //Se il giocatore ha segnato due caselle contigue col simbolo "X",
            e = player;                             //il computer ostacola la strategia dell'utente.    
            break;
        case ((cpu != null) && (player == null)):   //Se il computer ha segnato due caselle contigue col simbolo "O",
            e = cpu;                                //il computer vince la partita.
            break;
        case ((cpu != null) && (player != null)):   //Se entrambi il computer e l'utente hanno segnato due caselle
            e = cpu;                                //contigue con lo stesso simbolo, il computer vince la partita.
            break;
        default:
            e = progressLogic(a);                   //Se nessuna condizione è soddisfatta, il computer prosegue
            break;                                  //normalmente la sua strategia con la funzione "progressLogic".
    }
    return e;
}

function progressLogic(a){                               //Questa funzione contiene la logica che permette al computer di segnare
    let e;                                               //una casella vuota contigua a una casella segnata col simbolo "O" 
    switch (true){                                       //SOLO SE CI SONO 2 CASELLE VUOTE CONTIGUE E IN LINEA ALLA CASELLA PIENA COL SIMBOLO "O".
                                                         //Se non è disponibile nessuna opzione, la funzione estrae una casella random
                                                         //chiaramente verificando prima che sia vuota.
        case (a[0].textContent == "O"):                                             //|O|?|?|    |O| | |    |O| | |
            if (a[1].textContent == "" && a[2].textContent == ""){                  //| | | |    | |?| |    |?| | |
                e = getRandomIntInclusive(1, 2);                                    //| | | |    | | |?|    |?| | |
            } else if (a[4].textContent == "" && a[8].textContent == ""){
                e = getRandomIntInclusive(4, 8);
                while ((e == 5) || (e == 6) || (e == 7)){
                    e = getRandomIntInclusive(0, 8);
                }
            } else if (a[3].textContent == "" && a[6].textContent == ""){
                e = getRandomIntInclusive(3, 6);
                while ((e == 4) || (e == 5)){
                    e = getRandomIntInclusive(3, 6);
                }
            }    
            break;
        case (a[1].textContent == "O"):                                             //|?|O|?|    | |O| |
            if (a[0].textContent == "" && a[2].textContent == ""){                  //| | | |    | |?| |
                e = getRandomIntInclusive(0, 2);                                    //| | | |    | |?| |            
                while (e == 1){
                    e = getRandomIntInclusive(0, 2);
                }
            } else if (a[4].textContent == "" && a[7].textContent == ""){
                e = getRandomIntInclusive(4, 7);
                while ((e == 5) || (e == 6)){
                    e = getRandomIntInclusive(4, 7);
                }
            } else {
                e = progressLogic4(a);      //<-  <-  <-  <-  <-  <-  <-  <-  <-  <-  <-  <-  <-  <-
            }        
            break;
        case (a[2].textContent == "O"):                                             //|?|?|O|    | | |O|    | | |O| 
            if (a[0].textContent == "" && a[1].textContent == ""){                  //| | | |    | |?| |    | | |?|
                e = getRandomIntInclusive(0, 1);                                    //| | | |    |?| | |    | | |?|
            } else if (a[4].textContent == "" && a[6].textContent == ""){
                e = getRandomIntInclusive(4, 6);
                while ((e == 5)){
                    e = getRandomIntInclusive(4, 6);
                }
            } else if (a[5].textContent == "" && a[8].textContent == ""){
                e = getRandomIntInclusive(5, 8);
                while ((e == 6) || (e == 7)){
                    e = getRandomIntInclusive(5, 8);
                }
            }        
            break;
        case (a[3].textContent == "O"):                                             //|?| | |    | | | |
            if (a[0].textContent == "" && a[6].textContent == ""){                  //|O| | |    |O|?|?|
                e = getRandomIntInclusive(0, 6);                                    //|?| | |    | | | |
                while ((e == 1) || (e == 2) || (e == 3) || (e == 4) || (e == 5)){
                    e = getRandomIntInclusive(0, 6);
                }
            } else if (a[4].textContent == "" || a[5].textContent == ""){
                e = getRandomIntInclusive(4, 5);
            } else {
                e = progressLogic4(a);      //<-  <-  <-  <-  <-  <-  <-  <-  <-  <-  <-  <-  <-  <-
            }
            break;
        case (a[4].textContent == "O"):     //Per il caso 4 si può usare direttamente la funzione "progressLogic4".
            e = progressLogic4(a);          //<-  <-  <-  <-  <-  <-  <-  <-  <-  <-  <-  <-  <-  <-
            break;
        case (a[5].textContent == "O"):                                             //| | |?|    | | | |
            if (a[2].textContent == "" && a[8].textContent == ""){                  //| | |O|    |?|?|O|
                e = getRandomIntInclusive(2, 8);                                    //| | |?|    | | | |
                while ((e == 3) || (e == 4) || (e == 5) || (e == 6) || (e == 7)){
                    e = getRandomIntInclusive(2, 8);
                }
            } else if (a[3].textContent == "" && a[4].textContent == ""){
                e = getRandomIntInclusive(3, 4);
            }   
            break;
        case (a[6].textContent == "O"):                                             //| | | |    | | |?|    |?| | |
            if (a[7].textContent == "" && a[8].textContent == ""){                  //| | | |    | |?| |    |?| | |
                e = getRandomIntInclusive(7, 8);                                    //|O|?|?|    |O| | |    |O| | |
            } else if (a[2].textContent == "" && a[4].textContent == ""){
                e = getRandomIntInclusive(2, 4);
                while (e == 3){
                    e = getRandomIntInclusive(2, 4);
                }
            } else if (a[0].textContent == "" && a[3].textContent == ""){
                e = getRandomIntInclusive(0, 3);
                while ((e == 1) || (e == 2)){
                    e = getRandomIntInclusive(0, 3);
                }
            }       
            break;
        case (a[7].textContent == "O"):                                             //| | | |    | |?| |
            if (a[6].textContent == "" && a[8].textContent == ""){                  //| | | |    | |?| |
                e = getRandomIntInclusive(6, 8);                                    //|?|O|?|    | |O| |
                while (e == 7){
                    e = getRandomIntInclusive(6, 8);
                }
            } else if (a[1].textContent == "" && a[4].textContent == ""){
                e = getRandomIntInclusive(1, 4);
                while ((e == 2) || (e == 3)){
                    e = getRandomIntInclusive(2, 3);
                }
            }    
            break;
        case (a[8].textContent == "O"):                                             //| | | |    |?| | |    | | |?|
            if (a[6].textContent == "" && a[7].textContent == ""){                  //| | | |    | |?| |    | | |?|
                e = getRandomIntInclusive(6, 7);                                    //|?|?|O|    | | |O|    | | |O|
            } else if (a[0].textContent == "" && a[4].textContent == ""){
                e = getRandomIntInclusive(0, 4);
                while ((e == 1) || (e == 2) || (e == 3)){
                    e = getRandomIntInclusive(0, 4);
                }
            } else if (a[2].textContent == "" && a[5].textContent == ""){
                e = getRandomIntInclusive(2, 5);
                while ((e == 3) || (e == 4)){
                    e = getRandomIntInclusive(2, 5);
                }
            }     
            break;    //NOTA BENE: I casi (a[1] == "O") e (a[3] == "O") richiedono l'ausilio della funzione "progressLogic4" in quanto 1 e 3 sono minori di 4 e questo manda in crisi il programma,
        default:      //con la "progressLogic4" si permette al programma di utilizzare direttamente il caso (a[4] == "O")". SOTTO SONO MOSTRATE LE CONDIZIONI IMPOSSIBILI 1 e 3.
            break;    //    (a[1] == "O"):  |X|O|X|    (a[3] == "O"):  |X| | |     I casi (a[5] == "O") e (a[7] == "O") non richiedono
    }                 //                    | |O| |                    |O|O|X|     l'uso della funzione di supporto "progressLogic4".
                      //                    | |X| |                    |X| | |     poiché 5 e 7 sono maggiori di 4.
    if (e == undefined){                        //NOTA BENE: Con questa condizione si attiva l'estrazione casuale di un indice
        e = getRandomIntInclusive(0, 8);        //col controllo per evitare che l'indice estratto punti a una casella già segnata.
        while (a[e].textContent != ""){         //Questo è necessario in quanto se le caselle vuote sono poche (condizione di quasi fine partita), 
            e = getRandomIntInclusive(0, 8);    //lo switch case va in errore facendo risultare "e" come parametro undefined con conseguente
        }                                       //crash del programma.
    }
    return e;                  
}

function progressLogic4(a){     //Questa funzione è di supporto alla "progressLogic" qualora i casi (a[1] == "O") e (a[3] == "O") non fossero verificabili.
    let e;                                                                  //|?| | |    | |?| |    | | |?|    | | | |
    if (a[0].textContent == "" && a[8].textContent == ""){                  //| |O| |    | |O| |    | |O| |    |?|O|?|
        e = getRandomIntInclusive(0, 8);                                    //| | |?|    | |?| |    |?| | |    | | | |
        while ((e == 1) || (e == 2) || (e == 3) || (e == 4) || (e == 5) || (e == 6) || (e == 7)){
            e = getRandomIntInclusive(0, 8);
        }
    } else if (a[1].textContent == "" && a[7].textContent == ""){
        e = getRandomIntInclusive(1, 7);
        while ((e == 2) || (e == 3) || (e == 4) || (e == 5) || (e == 6)){
            e = getRandomIntInclusive(1, 7);
        }
    } else if (a[2].textContent == "" && a[6].textContent == ""){
        e = getRandomIntInclusive(2, 6);
        while ((e == 3) || (e == 4) || (e == 5)){
            e = getRandomIntInclusive(2, 6);
        }
    } else if (a[3].textContent == "" && a[5].textContent == ""){
        e = getRandomIntInclusive(3, 5);
        while (e == 4){
            e = getRandomIntInclusive(3, 5);
        }
    }
    return e;
}

function getRandomIntInclusive(min, max) {               //Questa funzione serve per l'estrazione di numeri casuali
    min = Math.ceil(min);                                                      //in un determinato range.
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1) + min);
}
/////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////PLAYER VS PLAYER/////////////////////////////////////////////
function playerVSplayer(){                                              //Funzione principale della modalità TWO PLAYERS.
    twoPlayers.setAttribute('style' , 'border: 3px solid yellow;');     //Aggiungo il bordo per indicare all'utente la modalità selezionata.
    playerVScpu.removeAttribute('style');                               //Rimuovo il bordo al bottone della modalità giocatore vs computer.
    hard.removeAttribute('style');                                //Rimuovo il bordo al bottone della modalità "hard".
                                                                                  
    playerVScpu.removeEventListener('click', playerVScomputer);         //Su questo bottone rimuovo l'eventListener per avviare la partita contro il computer.
    twoPlayers.removeEventListener('click', playerVSplayer);            //Su questo bottone rimuovo l'eventListener per avviare la partita contro un altro giocatore.
    hard.removeEventListener('click', engageHardMode);            //Su questo bottone rimuovo l'eventListener per avviare la partita in modalità "hard".

    playerVScpu.addEventListener('click', selectGame);                  //Su tutti e 3 i bottoni metto l'eventListener per tornare alla selezione della partita.
    twoPlayers.addEventListener('click', selectGame);
    hard.addEventListener('click', selectGame);

    if (GameCheck(buttons) == "unknown"){ 
        if(userMove == true){                                           //Quando move è true:
            let div = document.createElement('div');
            log.append(div);
            div.innerHTML = "Player 1, it's your turn.";                //Aggiungo la scritta per il giocatore1.
            buttons.forEach(button => {                                 //per ogni singolo bottone,
                button.removeEventListener('click', player2moves);      //rimuovo l'eventListener per il giocatore2 e
                button.addEventListener('click', player1moves);         //aggiungo l'eventListener per il giocatore1.
            })
        }
        if(userMove == false){                                              //Quando move è false:
            let div = document.createElement('div');
            log.append(div);
            div.innerHTML = "Player 2, it's your turn.";                //Aggiungo la scritta per il giocatore2.
            buttons.forEach(button => {                                 //per ogni singolo bottone,
                button.removeEventListener('click', player1moves);      //rimuovo l'eventListener per il giocatore1 e
                button.addEventListener('click', player2moves);         //aggiungo l'eventListener per il giocatore2.
            })
        }
    } else {
        buttons.forEach(button => {                                     //In caso di vincita rimuovo tutti gli eventListener possibili dai bottoni e
            button.removeEventListener('click', player1moves);
            button.removeEventListener('click', player2moves);
        })
        let div = document.createElement('div');                        //stampo su schermo la frase in base a chi ha vinto.
        switch (GameCheck(buttons)){           
            case "player":               
                log.append(div);
                div.innerHTML = "Player 1, you won! :)";
                break;
            case "computer":                                            //UTILIZZO COMUNQUE LA STRINGA "COMPUTER" PER EVITARE DI DOVER
                log.append(div);                                        //AGGIUNGERE UN ULTERIORE CASISTICA NELLA FUNZIONE GameCheck.
                div.innerHTML = "Player 2, you won. :)";
                break;
            case "noone":
                log.append(div);
                div.innerHTML = "Uhm, it's a draw. :/";
                break;
        }
    }
}
/////////////////////////////////////////////////////////////////////////////////////////////////
function GameCheck(a){                                  //Questa funzione controlla tutte le varie combinazioni in griglia
    let e;                                              //per verificare se uno dei due giocatori ha vinto o se c'è stato un pareggio.
    switch (true){                                      //Nel caso di vincita, manda il valore degli indici dei 3 bottoni a una funzione per colorarne i bordi.
        case (a[0].textContent != "" && a[0].textContent == a[1].textContent && a[1].textContent == a[2].textContent):
            if(a[0].textContent == "X"){
                e = "player";
            } else {
                e = "computer";
            }
            markWinnerButtons(0, 1, 2);
            break;
        case (a[3].textContent != "" && a[3].textContent == a[4].textContent && a[4].textContent == a[5].textContent):
            if(a[3].textContent == "X"){
                e = "player";
            } else {
                e = "computer";
            }
            markWinnerButtons(3, 4, 5);
            break;
        case (a[6].textContent != "" && a[6].textContent == a[7].textContent && a[7].textContent == a[8].textContent):
            if(a[6].textContent == "X"){
                e = "player";
            } else {
                e = "computer";
            }
            markWinnerButtons(6, 7, 8);
            break;
        case (a[0].textContent != "" && a[0].textContent == a[3].textContent && a[3].textContent == a[6].textContent):
            if(a[0].textContent == "X"){
                e = "player";
            } else {
                e = "computer";
            }
            markWinnerButtons(0, 3, 6);
            break;
        case (a[1].textContent != "" && a[1].textContent == a[4].textContent && a[4].textContent == a[7].textContent):
            if(a[1].textContent == "X"){
                e = "player";
            } else {
                e = "computer";
            }
            markWinnerButtons(1, 4, 7);
            break;
        case (a[2].textContent != "" && a[2].textContent == a[5].textContent && a[5].textContent == a[8].textContent):
            if(a[2].textContent == "X"){
                e = "player";
            } else {
                e = "computer";
            }
            markWinnerButtons(2, 5, 8);
            break;
        case (a[0].textContent != "" && a[0].textContent == a[4].textContent && a[4].textContent == a[8].textContent):
            if(a[0].textContent == "X"){
                e = "player";
            } else {
                e = "computer";
            }
            markWinnerButtons(0, 4, 8);
            break;
        case (a[2].textContent != "" && a[2].textContent == a[4].textContent && a[4].textContent == a[6].textContent):
            if(a[2].textContent == "X"){
                e = "player";
            } else {
                e = "computer";
            }
            markWinnerButtons(2, 4, 6);
            break;
        case (a[0].textContent != "" && a[1].textContent != "" && a[2].textContent != "" &&
              a[3].textContent != "" && a[4].textContent != "" && a[5].textContent != "" && 
              a[6].textContent != "" && a[7].textContent != "" && a[8].textContent != ""):
            e = "noone";
            break;
        default:
            e = "unknown";
            break;
    }              
    return e;
}
     
function markWinnerButtons(a, b, c){                       //Questa funzione colora i bordi dei bottoni vincenti.
    if (buttons[a].textContent == "X"){                     //Il bordo colorato svanisce grazie alla funzione resetButtons().
        buttons[a].removeAttribute('style');                //che si avvia tramite uno dei due bottoni di selezione modalità.
        buttons[b].removeAttribute('style');
        buttons[c].removeAttribute('style');
        buttons[a].setAttribute('style' , 'color: greenyellow; border: 4px solid cyan;');
        buttons[b].setAttribute('style' , 'color: greenyellow; border: 4px solid cyan;');
        buttons[c].setAttribute('style' , 'color: greenyellow; border: 4px solid cyan;');
    } else if (buttons[a].textContent == "O"){
        buttons[a].removeAttribute('style');
        buttons[b].removeAttribute('style');
        buttons[c].removeAttribute('style');
        buttons[a].setAttribute('style' , 'color: red; border: 4px solid cyan;');
        buttons[b].setAttribute('style' , 'color: red; border: 4px solid cyan;');
        buttons[c].setAttribute('style' , 'color: red; border: 4px solid cyan;');
    }
}

clearText.addEventListener('click', () => {                //Funzione che si attiva premendo il logo del cestino:
    log.innerHTML = "";                                    //serve per pulire tutto il testo di log.
    let div = document.createElement('div');          
    log.append(div);
    div.innerHTML = "Select mode ↑↑";
})

readInfo.addEventListener('click', () => {                 //Stampo a schermo un messaggio giusto per fare un po' di "scena".
    alert('v2.0\nWritten by Livio Bellini\nOn November 4th 2023\nWith love to Deloitte recruiters.');
})

selectGame();                                              //Funzione principale del gioco.